# Prerequisites


- Have a K8s Cluster with enough resources
- Knowledge about Kubernetes
- Knowledge about Kubernetes Operators

# Useful Links

- https://www.elastic.co/guide/en/cloud-on-k8s/current/index.html

# Workstation Configuration (if working on sela environments only)

1. Access to your workstation (the instructor will provide you with the connection details)

```
ssh sela@<your-ip>
```

2. Let's install kubectl in the workstation server

```
sudo snap install kubectl --classic
```

3. Let's use gcloud CLI to configure kubectl and get access to the cluster

```
gcloud container clusters get-credentials $(hostname) --zone $(gcloud compute instances list $(hostname) --format "value(zone)") --project devops-course-architecture
```

4. Test the kubectl configuration by running the following command

```
kubectl get nodes
```

# Instructions

We will deploy elasticsearch using ECK (Elastic Cloud on Kubernetes). It will include the following components: elasticsearch-operator, Elasticsearch and Kibana.


## Install the operator CRD's

1. Install custom resource definitions and the operator with its RBAC rules:

```
kubectl apply -f https://download.elastic.co/downloads/eck/1.3.1/all-in-one.yaml
```

2. Monitor the operator logs to make sure there are no errors:

```
kubectl -n elastic-system logs -f statefulset.apps/elastic-operator
```


## Deploy an Elasticsearch Cluster

- To deploy an elastic search cluster we will use the following command:

```
cat <<EOF | kubectl apply -f -
apiVersion: elasticsearch.k8s.elastic.co/v1
kind: Elasticsearch
metadata:
  name: elasticsearch
  namespace: default
spec:
  version: 7.10.2
  nodeSets:
  - name: default
    count: 3
    config:
      node.master: true
      node.data: true
      node.ingest: true
      node.store.allow_mmap: false
EOF
```

- Some important considerations:

  - We will create a single node elasticsearch for development purposes due we are using a single node kubernetes cluster
  - If your Kubernetes cluster does not have any Kubernetes nodes with at least 2GiB of free memory, the pod will be stuck in Pending state
  - Setting "node.store.allow_mmap: false" has performance implications and should be tuned for production workloads
  - This configuration only allocates a persistent volume of 1GiB for storage using the default storage class defined for the Kubernetes cluster

## Monitor the cluster health and creation process

- You can get an overall status using this command:

```
kubectl get elasticsearch
```

- You can track the cluster pods as well using the following command:

```
kubectl get pods --selector='elasticsearch.k8s.elastic.co/cluster-name=elasticsearch'
```

- And you can check the pod logs by running:

```
kubectl logs -f elasticsearch-es-default-0
```
```
kubectl logs -f elasticsearch-es-default-1
```
```
kubectl logs -f elasticsearch-es-default-2
```


## Access to elasticsearch

- A ClusterIP Service is automatically created for your cluster, get its IP by using:

```
kubectl get service elasticsearch-es-http
```

- To get the elasticsearch credentials (stored as kubernetes secret) use the following command:

```
kubectl get secret elasticsearch-es-elastic-user -o go-template='{{.data.elastic | base64decode}}'
```

- To access elasticsearch from inside the kubernetes cluster you can use:

```
curl -u "elastic:<PASSWORD>" -k "https://elasticsearch-es-http:9200"
```

- To access access elasticsearch from outside the kubernetes cluster you have 2 options:

  1. Using kubectl proxy

     - Start the proxy with the following command (in a separated terminal):

     ```
     kubectl port-forward service/elasticsearch-es-http 9200
     ```

     - Access to elasticsearch (from curl for example)

     ```
     curl -u "elastic:<PASSWORD>" -k "https://localhost:9200"
     ```

  2. Using a Load Balancer service

     - Create a LoadBalancer service to expose elasticsearch

     ```
     cat <<EOF | kubectl apply -f -
     apiVersion: v1
     kind: Service
     metadata:
       name: elasticsearch-public
       namespace: default
     spec:
       type: LoadBalancer
       selector:
         common.k8s.elastic.co/type: elasticsearch
         elasticsearch.k8s.elastic.co/cluster-name: elasticsearch
       ports:
         - port: 9200
           targetPort: 9200
     EOF
     ```

     - Retreive the external ip assigned by the LoadBalancer service (it will take a couple of minutes to be available)

     ```
     kubectl get service elasticsearch-public
     ``` 

     - Access to elasticsearch (from curl for example)

     ```
     curl -u "elastic:<PASSWORD>" -k "https://<external-ip>:9200"
     ```

## Deploy Kibana

- To deploy a kibana instance we will use the following command:

```
cat <<EOF | kubectl apply -f -
apiVersion: kibana.k8s.elastic.co/v1
kind: Kibana
metadata:
  name: kibana
  namespace: default
spec:
  version: 7.10.2
  count: 1
  elasticsearchRef:
    name: elasticsearch
EOF
```

## Monitor Kibana health and creation progress

- You can retrieve details about Kibana instances with the following command:

```
kubectl get kibana
```

- And the associated Pods:

```
kubectl get pod --selector='kibana.k8s.elastic.co/name=kibana'
```


## Access Kibana

- A ClusterIP Service is automatically created for Kibana, get its IP by using:

```
kubectl get service kibana-kb-http
```

- To get the kibana credentials use the following command (is the same one used to access elasticsearch):

```
kubectl get secret elasticsearch-es-elastic-user -o go-template='{{.data.elastic | base64decode}}'
```

- To access kibana from outside the kubernetes cluster you have 2 options:

  1. Using kubectl proxy

     - Start the proxy with the following command (in a separated terminal):

     ```
     kubectl port-forward service/kibana-kb-http 5601
     ```

     - Access to kibana from the browser:

     ```
     https://localhost:5601
     ```

  2. Using a LoadBalancer service

     - Create a LoadBalancer service to expose kibana

     ```
     cat <<EOF | kubectl apply -f -
     apiVersion: v1
     kind: Service
     metadata:
       name: kibana-public
       namespace: default
     spec:
       type: LoadBalancer
       selector:
         common.k8s.elastic.co/type: kibana
         kibana.k8s.elastic.co/name: kibana
       ports:
         - port: 5601
           targetPort: 5601
     EOF
     ```

     - Retreive the external ip assigned by the LoadBalancer service (it will take a couple of minutes to be available)

     ```
     kubectl get service kibana-public
     ``` 

     - Access to elasticsearch from the browser

     ```
     https://<external-ip>:5601
     ```

## Update the cluster configuration

You can add and modify most elements of the original cluster specification provided that they translate to valid transformations of the underlying Kubernetes resources. The operator will attempt to apply your changes with minimal disruption to the existing cluster.

- For example, you can scale up the cluster by modifying the elasticsearch resource as shown below

```
cat <<EOF | kubectl apply -f -
apiVersion: elasticsearch.k8s.elastic.co/v1
kind: Elasticsearch
metadata:
  name: elasticsearch
  namespace: default
spec:
  version: 7.10.2
  nodeSets:
  - name: default
    count: 4
    config:
      node.master: true
      node.data: true
      node.ingest: true
      node.store.allow_mmap: false
EOF
```

- Check this link for more advanced configuration examples:

```
https://github.com/elastic/cloud-on-k8s/tree/master/config/samples
```
